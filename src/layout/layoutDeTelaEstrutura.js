import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View,  ScrollView} from "react-native";

export default function Layout() {
  return (
    <View style={styles.container}>
      
      <View style={styles.header}>
        <Text>MENU</Text>
      </View>

      <View style={styles.content}>
      <ScrollView>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
      <Text style={styles.text}>CONTEUDO</Text>
     
      </ScrollView>
      </View>

      <View style={styles.footer}>
        <Text>RODAPE</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:"column",
    justifyContent: "space-between",
  },
  header:{
    height:50,
    backgroundColor:'#C6A6E5'

  },
  content:{
    flex:1,
    backgroundColor:'#DDD5E3'

  },
  footer:{
    height:50,
    backgroundColor:'#C6A6E5'

  },
  text:{
    fontSize:10,
    fontWeight:'bold',
    textAlign:'center'

  }
});
