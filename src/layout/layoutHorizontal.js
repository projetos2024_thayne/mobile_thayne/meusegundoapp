import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import Layout from "./layoutDeTelaEstrutura";

export default function LayoutHorizontal() {
  return (
    <View style={styles.container}>
      <ScrollView horizontal={true}>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
        <View style={styles.box3}></View>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
        <View style={styles.box3}></View>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
        <View style={styles.box3}></View>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
        <View style={styles.box3}></View>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
        <View style={styles.box3}></View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "black",
  },
  box1: {
    marginTop: 20,
    width: 50,
    height: 50,
    backgroundColor: "#D7FFFD",
    borderRadius: 100,
  },
  box2: {
    marginTop: 20,
    width: 50,
    height: 50,
    backgroundColor: "#A1E6E3",
    borderRadius: 100,
  },
  box3: {
    marginTop: 20,
    width: 50,
    height: 50,
    backgroundColor: "#8FB9AA",
    borderRadius: 100,
  },
});
