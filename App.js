import { StatusBar } from "expo-status-bar";
import Layout from "./src/layout/layoutDeTelaEstrutura";
import LayoutHorizontal from "./src/layout/layoutHorizontal";
import LayoutGrade from "./src/layout/layoutGrade";
import Components from "./src/components/components";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import SegundaTela from "./src/segundaTela";
import TelaInicial from "./src/telaInicial";
import Menu from "./src/components/menu";

const Stack = createStackNavigator();
export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Menu" component={Menu} />
        <Stack.Screen name="TelaInicial" component={TelaInicial} />
        <Stack.Screen name="SegundaTela" component={SegundaTela} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}